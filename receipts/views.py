from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# show user's whole receipt list.
@login_required
def receiptlist(request):
    receipt_lst = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list_view": receipt_lst,
    }
    return render(request, "receipts/receipt_list.html", context)


# user can create a receipt
@login_required
def create_receipt(request):
    if request.method == "POST":
        # we use the form to validate the values
        # and save them to the DB
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            # if all goes well, it is submitted and
            # client is rediected to home
            return redirect("home")
    else:
        # create instance of django model form class
        form = ReceiptForm()
    # put the form in the context
    context = {
        "form": form,
    }

    # render the HTML template with the form
    return render(request, "receipts/create.html", context)


# user can create a category view
@login_required
def create_category(request):
    if request.method == "POST":
        # we use the form to validate the values
        # and save them to the DB
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            # if all goes well, recipe is submitted and
            # client is rediected to recipe_list(homepage)
            return redirect("category_list")
    else:
        # create instance of django model form class
        form = ExpenseCategoryForm()
    # put the form in the context
    context = {
        "form": form,
    }

    # render the HTML template with the form
    return render(request, "receipts/create_category.html", context)


# show the logged in user's categories
@login_required
def my_categories(request):
    mine = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": mine,
    }
    return render(request, "receipts/my_categories.html", context)


# show the logged in uer's bank accounts
@login_required
def my_accounts(request):
    my_acct = Account.objects.filter(owner=request.user)
    context = {
        "accounts": my_acct,
    }
    return render(request, "receipts/my_accounts.html", context)


# user can create new bank account
@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            # if all goes well, recipe is submitted and client is rediected
            return redirect("account_list")
    else:
        # create instance of django model form class
        form = AccountForm()
    # put the form in the context
    context = {
        "form": form,
    }

    # render the HTML template with the form
    return render(request, "receipts/create_account.html", context)
