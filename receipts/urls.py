from django.urls import path
from receipts.views import (
    receiptlist,
    create_receipt,
    my_categories,
    my_accounts,
    create_category,
    create_account,
)


urlpatterns = [
    path("", receiptlist, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", my_categories, name="category_list"),
    path("accounts/", my_accounts, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
