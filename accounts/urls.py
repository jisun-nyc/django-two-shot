from accounts.views import (
    user_login,
    user_logout,
    create_signup,
)
from django.urls import path

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", create_signup, name="signup"),
]
